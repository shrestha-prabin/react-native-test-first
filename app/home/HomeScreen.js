import React, { Component } from 'react';
import GridView from 'react-native-gridview';
import Row from './Row';

listContents = [
    {
        id: 'consonant',
        title: 'व्यञ्नजन वर्ण',
        imageUri: require('./home_images/consonant.png')
    },
    {
        id: 'vowel',
        title: 'स्वर वर्ण',
        imageUri: require('./home_images/vowel.png')
    },
    {
        id: 'day',
        title: 'बार',
        imageUri: require('./home_images/day.png')
    },
    {
        id: 'month',
        title: 'महीना',
        imageUri: require('./home_images/month.png')
    },
    {
        id: 'season',
        title: 'ऋतुहरू',
        imageUri: require('./home_images/season.png')
    },
    {
        id: 'fruit',
        title: 'फलफुल',
        imageUri: require('./home_images/fruit.png')
    },
    {
        id: 'vegetable',
        title: 'तरकारी',
        imageUri: require('./home_images/vegetable.png')
    },
    {
        id: 'animal',
        title: 'जनावरहरू',
        imageUri: require('./home_images/animal.png')
    },
    {
        id: 'bird',
        title: 'चराचुरुङ्गी',
        imageUri: require('./home_images/bird.png')
    },
    {
        id: 'anthem',
        title: 'राष्ट्रिय गान',
        imageUri: require('./home_images/anthem.png')
    }
];

const HomeScreen = () => {
    var id;
    return (
        <GridView data={listContents}
            itemsPerRowPotrait={1}
            itemsPerRowLandscape={2}
            renderItem={(rowItem) => {
                return (
                    <Row rowItem={rowItem} />
                )
            }}
        />
    );

}

export default HomeScreen;