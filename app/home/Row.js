import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native';
import Dimensions from 'Dimensions';

import { Actions } from 'react-native-router-flux';

const screen = Dimensions.get('window');

class Row extends Component {
    render({ rowItem } = this.props) {
        const { id, title, imageUri } = rowItem;

        return (
            <TouchableOpacity style={styles.container}
                activeOpacity={0.8}
                delayPressOut={0}
                onPress={() => this.openScreen(id)}>
                <ImageBackground source={imageUri} style={styles.image}>
                    <Text style={styles.text}>{title}</Text>
                </ImageBackground>
            </TouchableOpacity>
        )
    }

    openScreen = (id) => {
        // console.log(id)
        switch (id) {
            case 'consonant':
                Actions.consonant();
                break;
            case 'vowel':
                Actions.vowel();
                break;
            case 'day':
                Actions.day();
                break;
            case 'month':
                Actions.month();
                break;
            case 'season':
                Actions.season();
                break;
            case 'fruit':
                Actions.fruit();
                break;
            case 'vegetable':
                Actions.vegetable();
                break;
            case 'animal':
                Actions.animal();
                break;
            case 'bird':
                Actions.bird();
                break;
            case 'anthem':
                Actions.anthem();
                break;
        }
    }
}

export default Row;

const styles = StyleSheet.create({
    container: {
        marginTop: 4
    },
    text: {
        fontSize: 30,
        color: '#fff',
        fontWeight: 'bold',
        textShadowColor: '#000',
        textShadowRadius: 20,
        textShadowOffset: { width: 1, height: 1 },
        justifyContent: 'center'
    },
    image: {
        height: 150,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    }
});