import React, { Component } from 'react';

import { StyleSheet, View, Text } from 'react-native';
import MyGrid from './../GridViewDisplay'

const birds = [
    { id: 'crow', title: 'काग', imageUri: require('./imgs/crow.png') },
    { id: 'duck', title: 'हाँस', imageUri: require('./imgs/duck.png') },
    { id: 'hen', title: 'कुखुरा', imageUri: require('./imgs/hen.png') },
    { id: 'owl', title: 'उल्लु', imageUri: require('./imgs/owl.png') },
    { id: 'peacock', title: 'मयुर', imageUri: require('./imgs/peacock.png') },
    { id: 'pigeon', title: 'परेवा', imageUri: require('./imgs/pigeon.png') },
    { id: 'cuckoo', title: 'काेइली', imageUri: require('./imgs/robin.png') }
];

class BirdScreen extends Component {

    render() {
        return (
            <View>
                <MyGrid data={birds} />
            </View>
        )
    }
}

export default BirdScreen;