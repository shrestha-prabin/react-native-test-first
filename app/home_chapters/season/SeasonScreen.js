import React, { Component } from 'react';

import ImageWordView from './../ImageWordView';

class SeasonScreen extends Component {

    seasons = ['बसन्त', 'ग्रष्म', 'बर्षा', 'शरद', 'हेमन्त', 'शिशिर'];

    images = [
        require('./imgs/s1.png'), require('./imgs/s2.png'), require('./imgs/s3.png'),
        require('./imgs/s4.png'), require('./imgs/s5.png'), require('./imgs/s6.png')];

    render() {
        return (
            <ImageWordView
                dataList={this.seasons}
                dataImages={this.images}
                soundFileInitial='s' />
        )
    }
}

export default SeasonScreen;