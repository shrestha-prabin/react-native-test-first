import React, { Component } from 'react';

import { StyleSheet, View, Text } from 'react-native';
import MyGrid from './../GridViewDisplay'

const animals = [
    { id: 'bear', title: 'भालु', imageUri: require('./imgs/bear.png') },
    { id: 'buffalo', title: 'भैँसी', imageUri: require('./imgs/buffalo.png') },
    { id: 'bat', title: 'चमेराे', imageUri: require('./imgs/bat.png') },
    { id: 'cow', title: 'गाई', imageUri: require('./imgs/cow.png') },
    { id: 'dog', title: 'कुकुर', imageUri: require('./imgs/dog.png') },
    { id: 'elephant', title: 'हात्ती', imageUri: require('./imgs/elephant.png') },
    { id: 'goat', title: 'बाख्रा', imageUri: require('./imgs/goat.png') },
    { id: 'gorilla', title: 'गोरिल्ला', imageUri: require('./imgs/gorilla.png') },
    { id: 'horse', title: 'घोडा', imageUri: require('./imgs/horse.png') },
    { id: 'monkey', title: 'बाँदर', imageUri: require('./imgs/monkey.png') },
    { id: 'rabbit', title: 'खरायो', imageUri: require('./imgs/rabbit.png') },
    { id: 'tiger', title: 'बाघ', imageUri: require('./imgs/tiger.png') },
    { id: 'yak', title: 'चाैरी गाई', imageUri: require('./imgs/yak.png') },
];

class AnimalScreen extends Component {

    render() {
        return (
            <View>
                <MyGrid data={animals} />
            </View>
        )
    }
}

export default AnimalScreen;