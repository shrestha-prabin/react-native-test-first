import React, { Component } from 'react';

import ImageWordView from './../ImageWordView';

class MonthScreen extends Component {

    months = ['बैशाख', 'जेठ', 'असार', 'साउन', 'भदै', 'असाेज',
        'कार्तिक', 'मङ्सिर', 'पुस', 'माध', 'फाल्गुन', 'चैत'];

    images = [
        require('./imgs/m1.png'), require('./imgs/m2.png'), require('./imgs/m3.png'), require('./imgs/m4.png'),
        require('./imgs/m5.png'), require('./imgs/m6.png'), require('./imgs/m7.png'), require('./imgs/m8.png'),
        require('./imgs/m9.png'), require('./imgs/m10.png'), require('./imgs/m11.png'), require('./imgs/m12.png'),
    ];

    render() {
        return (
            <ImageWordView
                dataList={this.months}
                dataImages={this.images}
                soundFileInitial='m' />
        )
    }
}

export default MonthScreen;