import React, { Component } from 'react';

import { StyleSheet, View, Text } from 'react-native';
import CustomGridLayout from './../GridViewDisplay'

const fruits = [
    { id: 'apple', title: 'स्याउ', imageUri: require('./imgs/apple.png') },
    { id: 'banana', title: 'केरा', imageUri: require('./imgs/banana.png') },
    { id: 'grape', title: 'अंगूर', imageUri: require('./imgs/grape.png') },
    { id: 'mango', title: 'अाँप', imageUri: require('./imgs/mango.png') },
    { id: 'orange', title: 'सुन्तला', imageUri: require('./imgs/orange.png') },
    { id: 'pomegranete', title: 'अनार', imageUri: require('./imgs/pomegranete.png') }
];

class FruitScreen extends Component {

    render() {
        return (
            <View>
                <CustomGridLayout data={fruits} />
            </View>
        )
    }
}

export default FruitScreen;