import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, ScrollView } from 'react-native';
import Sound from 'react-native-sound';

class DayRow extends Component {

    render() {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={this.props.onPress}
                style={[styles.row, { backgroundColor: this.props.backgroundColor }]}>

                <Text style={[styles.text, { color: this.props.textColor }]}>{this.props.text}</Text>

            </TouchableOpacity>
        )
    };
}

class DayScreen extends Component {

    render() {
        return (
            <ScrollView style={styles.container}>
                <DayRow backgroundColor='#fff' text='अाइतबार' onPress={() => this.playAudio('d1.m4a')} />
                <DayRow backgroundColor='#fff' text='साेमबार' onPress={() => this.playAudio('d2.m4a')} />
                <DayRow backgroundColor='#fff' text='मंगलबार' onPress={() => this.playAudio('d3.m4a')} />
                <DayRow backgroundColor='#fff' text='बुधबार' onPress={() => this.playAudio('d4.m4a')} />
                <DayRow backgroundColor='#fff' text='बिहिबार' onPress={() => this.playAudio('d5.m4a')} />
                <DayRow backgroundColor='#fff' text='शुक्रबार' onPress={() => this.playAudio('d6.m4a')} />
                <DayRow backgroundColor='#c1001a' text="शनिबार" textColor='#fff'
                    onPress={() => this.playAudio('d7.m4a')} />
            </ScrollView>
        )
    }

    playAudio(file) {
        const sound = new Sound(file, Sound.MAIN_BUNDLE, (error) => {
            if (error) { return; }
            sound.play((success) => {
                if (success) sound.release();
            });
        });
    }
}

export default DayScreen;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#eee'
    },
    row: {
        height: 65,
        width: '80%',
        alignSelf: 'center',
        marginTop: 6,
        marginBottom: 6,
        borderRadius: 5,
        elevation: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        flex: 1,
        fontSize: 20,
        textAlignVertical: 'center'
    }
})