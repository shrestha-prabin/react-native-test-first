import React, { Component } from 'react';

import { StyleSheet, StatusBar, View, Text, Image, Button, Animated, Easing, ImageBackground, TouchableNativeFeedback, TouchableOpacity } from 'react-native';

import { Actions } from 'react-native-router-flux';


class ImageWordView extends Component {

    componentDidMount() {
        StatusBar.setHidden(true);
    }

    render() {
        return (
            <ImageBackground
                style={styles.container}
                source={require('./imgs/main-background.png')}>

                <Sun
                    style={styles.sunStyle}
                    source={require('./imgs/sun.png')}
                    title={'पढार्इ सिकार्इ'}
                    onPress={() => Actions.home()} />

                <Cloud
                    style={styles.cloudStyle}
                    source={require('./imgs/cloud-moving.png')}
                    title='मनाेरञ्जन'
                    onPress={() => Actions.games()} />
            </ImageBackground>
        )
    }
}

export default ImageWordView;

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    sunStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        height: 200,
        width: 200,
    },
    cloudStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 200,
        width: 250,
    },
})

class Sun extends Component {

    constructor() {
        super();
        this.scaleValue = new Animated.Value(0);
        this.touchScaleValue = new Animated.Value(0);
    }

    componentDidMount() {
        this.scale();
    }

    scale() {
        this.scaleValue.setValue(0);
        Animated.timing(
            this.scaleValue,
            {
                toValue: 1,
                duration: 6000,
                easing: Easing.linear()
            }
        ).start(() => this.scale());
    }
    animatePressIn=()=> {
        this.touchScaleValue.setValue(0);
        Animated.timing(
            this.touchScaleValue,
            {
                toValue: 1,
                duration: 200,
            }).start();
    }
    animatePressOut=()=> {
        this.touchScaleValue.setValue(1);
        Animated.timing(
            this.touchScaleValue,
            {
                toValue: 0,
                duration: 200,
            }).start();
    }
    render() {

        const scale = this.scaleValue.interpolate({
            inputRange: [0, 0.6, 1],
            outputRange: [1, 1.1, 1]
        });

        const touchScale = this.touchScaleValue.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0.95]
        });

        const styles = StyleSheet.create({
            image: {
                resizeMode: 'contain',
                height: '100%',
                width: '100%',
            },
            text: {
                position: 'absolute',
                fontSize: 30,
                color: '#ad7063',
                fontWeight: 'bold'
            }
        })

        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                activeOpacity={0.7}
                onPressIn={this.animatePressIn}
                onPressOut={this.animatePressOut}>
                <Animated.View
                    style={[{
                        transform: [{ scale: scale }],
                    },
                    this.props.style,]}
                    source={this.props.source}>

                    <Animated.Image
                        style={[styles.image, {transform: [{scale: touchScale}]}]}
                        source={this.props.source} />

                    <Text
                        style={styles.text}>{this.props.title}</Text>

                </Animated.View>
            </TouchableOpacity>
        )
    }
}

class Cloud extends Component {

    constructor() {
        super();
        this.translateXvalue = new Animated.Value(0);
        this.touchScaleValue = new Animated.Value(0);
    }

    componentDidMount() {
        this.move();
    }

    move() {
        this.translateXvalue.setValue(0);

        Animated.timing(
            this.translateXvalue,
            {
                toValue: 1,
                duration: 8000,
                easing: Easing.linear()
            }
        ).start(() => this.move());
    }

    
    animatePressIn=()=> {
        this.touchScaleValue.setValue(0);
        Animated.timing(
            this.touchScaleValue,
            {
                toValue: 1,
                duration: 200,
            }).start();
    }
    animatePressOut=()=> {
        this.touchScaleValue.setValue(1);
        Animated.timing(
            this.touchScaleValue,
            {
                toValue: 0,
                duration: 200,
            }).start();
    }
    render() {

        const move = this.translateXvalue.interpolate({
            inputRange: [0, 0.4, 1],
            outputRange: [0, 100, 0]
        });

        const touchScale = this.touchScaleValue.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0.95]
        });
        
        const styles = StyleSheet.create({

            image: {
                height: '100%',
                width: '100%',
                resizeMode: 'contain'
            },
            text: {
                position: 'absolute',
                fontSize: 30,
                color: '#34ad34',
                fontWeight: 'bold'
            }
        })

        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                activeOpacity={0.7}
                onPressIn={this.animatePressIn}
                onPressOut={this.animatePressOut}>
                <Animated.View
                    style={[{
                        transform: [{ translateX: move }],
                    },
                    this.props.style,
                    ]}
                    source={this.props.source}>

                    <Animated.Image
                        style={[styles.image, {transform: [{scale: touchScale}]}]}
                        source={this.props.source} />

                    <Text
                        style={styles.text}>{this.props.title}</Text>

                </Animated.View>
            </TouchableOpacity>
        )
    }
}
