export default data = [
    { image: require('../../home_chapters/consonant/imgs/a1.png'), name: ['क', 'म', 'ल'] },
    { image: require('../../home_chapters/consonant/imgs/a2.png'), name: ['ख', 'रा', 'यो'] },
    { image: require('../../home_chapters/consonant/imgs/a3.png'), name: ['ग', 'म', 'ला'] },
    { image: require('../../home_chapters/consonant/imgs/a4.png'), name: ['घ', 'र'] },
    { image: require('../../home_chapters/consonant/imgs/a6.png'), name: ['च', 'रा'] },
    { image: require('../../home_chapters/consonant/imgs/a7.png'), name: ['छा', 'ता'] },
    { image: require('../../home_chapters/consonant/imgs/a8.png'), name: ['ज', 'हा', 'ज'] },
    { image: require('../../home_chapters/consonant/imgs/a9.png'), name: ['झ', 'र', 'णा'] },
    { image: require('../../home_chapters/consonant/imgs/a11.png'), name: ['ट', 'मा', 'ट', 'र'] },
    { image: require('../../home_chapters/consonant/imgs/a12.png'), name: ['ठे', 'की'] },
    { image: require('../../home_chapters/consonant/imgs/a13.png'), name: ['ड', 'म', 'रू'] },
    { image: require('../../home_chapters/consonant/imgs/a14.png'), name: ['ढ', 'क'] },
    { image: require('../../home_chapters/consonant/imgs/a16.png'), name: ['त', 'ब', 'ला'] },
    { image: require('../../home_chapters/consonant/imgs/a18.png'), name: ['द', 'म', 'क', 'ल'] },
    { image: require('../../home_chapters/consonant/imgs/a19.png'), name: ['ध', 'र', 'ह', 'रा'] },
    { image: require('../../home_chapters/consonant/imgs/a20.png'), name: ['न', 'रि', 'व', 'ल'] },
];