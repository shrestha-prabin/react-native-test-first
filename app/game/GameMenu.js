import React, { Component } from 'react';

import { StyleSheet, View, Button, Image, Text, ImageBackground, TouchableOpacity, Animated } from 'react-native';

import { Actions } from 'react-native-router-flux';

class GameMenu extends Component {

    render() {
        return (
            <ImageBackground
                style={styles.container}
                source={require('./../imgs/main-background.png')}>

                <GameButton
                    title={'चित्र' + '\n' + 'चिन'}
                    textColor='#fff'
                    source={require('./namePicture/name-picture.png')}
                    onPress={() => Actions.namePicture()}
                />
                <GameButton
                    title={'सम्झेर' + '\n' + 'खेल'}
                    textColor='#ddd'
                    source={require('./pairPicture/pair-picture.png')}
                    onPress={() => Actions.pairPicture()}
                />
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    }
})

class GameButton extends Component {

    constructor() {
        super();
        this.scaleValue = new Animated.Value(0);
    }

    animatePressIn=()=> {
        this.scaleValue.setValue(0);
        Animated.timing(
            this.scaleValue,
            {
                toValue: 1,
                duration: 200,
            }).start();
    }
    animatePressOut=()=> {
        this.scaleValue.setValue(1);
        Animated.timing(
            this.scaleValue,
            {
                toValue: 0,
                duration: 150,
            }).start();
    }

    render() {

        const scale = this.scaleValue.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0.95]
        })

        const styles = StyleSheet.create({
            container: {
                justifyContent: 'center',
                alignItems: 'center',
                flex: 0.5,
                width: 300,
            },
            image: {
                flex: 1,
                resizeMode: 'contain',
            },
            text: {
                fontSize: 30,
                fontWeight: 'bold',
                position: 'absolute',
                color: '#fff',
                textAlign: 'center',
                textShadowColor: '#000',
                textShadowRadius: 3,
                textShadowOffset: { width: 2, height: 2 }
            }
        })
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                onPressIn={this.animatePressIn}
                onPressOut={this.animatePressOut}
                activeOpacity={0.9}
            >
                <Animated.View
                    style={[styles.container, {transform: [{scale: scale}]}]}
                >
                    <Image
                        style={styles.image}
                        source={this.props.source}
                    />
                    <Text
                        style={[styles.text, {color: this.props.textColor}]}
                    >{this.props.title}</Text>
                </Animated.View>

            </TouchableOpacity>
        )
    }
}

export default GameMenu;