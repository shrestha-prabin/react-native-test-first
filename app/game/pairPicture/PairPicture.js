import React, { Component } from 'react';

import { StyleSheet, View, Alert, Text, Dimensions, Image, TouchableOpacity, ToastAndroid, Animated, Easing } from 'react-native';

import { Actions } from 'react-native-router-flux';

const images = [
    require('../../home_chapters/bird/imgs/sparrow.png'),
    require('../../home_chapters/consonant/imgs/a2.png'),
    require('../../home_chapters/consonant/imgs/a3.png'),
    require('../../home_chapters/consonant/imgs/a4.png'),
    require('../../home_chapters/consonant/imgs/a25.png'),
    require('../../home_chapters/consonant/imgs/a6.png'),
];
const defaultImage = require('./question.png');
const transparentImage = require('./blank.png');

const data = [
    images[0], images[1], images[2], images[3],
    images[4], images[5], images[0], images[1],
    images[2], images[3], images[4], images[5]
];

var currentGameBoard = [];
var gameBoard = [
    false, false, false, false,
    false, false, false, false,
    false, false, false, false,
];
var displayImage = [];

class PairPicture extends Component {

    constructor() {
        super();

        currentGameBoard = this.shuffle(data);

        for (i = 0; i < 12; i++) {
            displayImage[i] = defaultImage;
        }

        this.state = ({
            time: 0,
            isFirstClicked: false,
            isSecondClicked: false,
            firstClickItem: -1,
            secondClickItem: -1
        });
    }

    componentDidMount() {
        setInterval(() => {
            let t = this.state.time;
            t++;
            this.setState({
                time: t
            })
        }, 1000);
    }

    componentWillUnmount() {
        this.resetGame();
    }

    handleItemClick = (i) => {

        //hanlde click of first item
        if (!this.state.isFirstClicked) {
            displayImage[i] = currentGameBoard[i];
            gameBoard[i] = true;                //disables click also

            this.setState({
                isFirstClicked: true,
                firstClickItem: i,
            });

            return;
        }

        //handle click of second item
        displayImage[i] = currentGameBoard[i];
        gameBoard[i] = true;

        this.setState({
            isSecondClicked: true,
            secondClickItem: i,
        });

        //temporarily disable all tiles until timeout
        let tempGameBoard = gameBoard;
        gameBoard = [
            true, true, true, true,
            true, true, true, true,
            true, true, true, true,
        ];

        setTimeout(() => {
            let i1 = this.state.firstClickItem;
            let i2 = this.state.secondClickItem;
            if (currentGameBoard[i1] == currentGameBoard[i2]) {
                //ToastAndroid.show('Match at ' + i1 + i2, ToastAndroid.SHORT);

                displayImage[i1] = transparentImage;
                displayImage[i2] = transparentImage;

                tempGameBoard[i1] = true;
                tempGameBoard[i2] = true;
            } else {
                //ToastAndroid.show('No Match ' + i1 + i2, ToastAndroid.SHORT);

                displayImage[i1] = defaultImage;
                displayImage[i2] = defaultImage;

                tempGameBoard[i1] = false;
                tempGameBoard[i2] = false;
            }

            this.setState({
                isFirstClicked: false,
                isSecondClicked: false,
                firstClickItem: -1,
                secondClickItem: -1
            });
            gameBoard = tempGameBoard;

            //check if all tiles have been matched
            let flag = 1;
            for (i = 0; i < 12; i++) {
                if (gameBoard[i] == false) {
                    flag = 0;
                    break;
                }
            }

            if (flag == 1) {
                this.finishGame();
            }
        }, 400);
    }



    finishGame = () => {

        let t = this.state.time;
        Alert.alert(
            '',
            'समय : ' + t,
            [
                {
                    text: 'फेरी खेल्ने', onPress: () => {
                        this.resetGame();
                    }
                },
                {
                    text: 'बन्द गर', onPress: () => {
                        Actions.pop();
                    }
                }
            ]
        )
    }

    resetGame = () => {

        currentGameBoard = this.shuffle(data);

        gameBoard = [
            false, false, false, false,
            false, false, false, false,
            false, false, false, false,
        ];

        for (i = 0; i < 12; i++) {
            displayImage[i] = defaultImage;
        }

        this.setState({
            time: 0
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.timerText}> {'समय : ' + this.state.time}</Text>

                <View style={styles.tileContianer}>
                    <View style={styles.horizontal}>
                        <ImageUnit
                            onPress={() => this.handleItemClick(0)}
                            source={displayImage[0]}
                            disabled={gameBoard[0]} />
                        <ImageUnit
                            onPress={() => this.handleItemClick(1)}
                            source={displayImage[1]}
                            disabled={gameBoard[1]} />
                        <ImageUnit
                            onPress={() => this.handleItemClick(2)}
                            source={displayImage[2]}
                            disabled={gameBoard[2]} />
                        <ImageUnit
                            onPress={() => this.handleItemClick(3)}
                            source={displayImage[3]}
                            disabled={gameBoard[3]} />
                    </View>

                    <View style={styles.horizontal}>
                        <ImageUnit
                            onPress={() => this.handleItemClick(4)}
                            source={displayImage[4]}
                            disabled={gameBoard[4]} />
                        <ImageUnit
                            onPress={() => this.handleItemClick(5)}
                            source={displayImage[5]}
                            disabled={gameBoard[5]} />
                        <ImageUnit
                            onPress={() => this.handleItemClick(6)}
                            source={displayImage[6]}
                            disabled={gameBoard[6]} />
                        <ImageUnit
                            onPress={() => this.handleItemClick(7)}
                            source={displayImage[7]}
                            disabled={gameBoard[7]} />
                    </View>

                    <View style={styles.horizontal}>
                        <ImageUnit
                            onPress={() => this.handleItemClick(8)}
                            source={displayImage[8]}
                            disabled={gameBoard[8]} />
                        <ImageUnit
                            onPress={() => this.handleItemClick(9)}
                            source={displayImage[9]}
                            disabled={gameBoard[9]} />
                        <ImageUnit
                            onPress={() => this.handleItemClick(10)}
                            source={displayImage[10]}
                            disabled={gameBoard[10]} />
                        <ImageUnit
                            onPress={() => this.handleItemClick(11)}
                            source={displayImage[11]}
                            disabled={gameBoard[11]} />
                    </View>
                </View>
            </View>
        )
    }

    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }
}

export default PairPicture;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    tileContianer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    horizontal: {
        flexDirection: 'row'
    },
    timerText: {
        fontSize: 24,
        color: '#222'
    }
})

class ImageUnit extends Component {
    render() {
        let imageSize = Math.min(Dimensions.get('window').width, Dimensions.get('window').height) * 0.23;

        return (
            <TouchableOpacity
                style={{
                    height: imageSize, width: imageSize,
                    backgroundColor: '#99ad99', elevation: 2, borderRadius: 5,
                    margin: 2
                }}
                onPress={this.props.onPress}
                activeOpacity={0.6}
                disabled={this.props.disabled}
            >
                <Image
                    style={{
                        height: imageSize, width: imageSize, resizeMode: 'contain',
                    }}
                    source={this.props.source}
                    fadeDuration={0} />
            </TouchableOpacity>
        )
    }
}